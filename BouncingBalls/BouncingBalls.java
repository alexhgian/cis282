import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.BorderLayout;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.Color;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

import java.util.ArrayList;//Found example here: http://www.anyexample.com/programming/java/java_arraylist_example.xml
//Did not use this yet.

import java.awt.event.MouseListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import java.util.Random;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;


public class BouncingBalls
{

	public static void main(String args[])
	{
		JFrame frame = new JFrame("Bouncing Balls v0.l");
		BallPanel app = new BallPanel();
		
		frame.add(app);
		frame.add(app.createStatusBar(),BorderLayout.SOUTH);
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.setSize(500,500);
      frame.setVisible( true );
	}

}
/********************************************************************

********************************************************************/
class BallPanel extends JPanel
{	 
	Dimension panelDimension;
	
	Ball[] ballArray = new Ball[1000];
	ExecutorService threadExecutor = Executors.newCachedThreadPool();
	
	boolean ballStarted = false;
	
	private String statusMessage;
   private  JLabel statusBar;
	private static final int UPDATE_RATE = 30; 
	int ballCount = 0;
	
	public BallPanel()//Constructor
	{
		addMouseMotionListener( new MyMouseListener() );
		addMouseListener( new MouseReleaseHandler() );
		this.addComponentListener( new resizeHandler() );
		
	}	
	
	public void paintComponent(Graphics g){
		super.paintComponent(g); 
		
		if (ballStarted == true) {
		
			for (int i = 0; i < ballCount; i++){
				ballArray[i].draw(g);
			}
			
		}
		
		if( isMouseDragged == true ) {
			g.drawLine(x1,y1,x2,y2);
		}
		
		
		
	}	

	public void startBounce(){
	   Thread bounceThread = new Thread() {
         public void run() {
            while (true) {             
					for (int i = 0; i < ballCount; i++){
						ballArray[i].setBallArray(ballArray);
					}   
				
					for (int i = 0; i < ballCount; i++){
						ballArray[i].updatePosition();
					}     
						
							
               repaint();      
               try {
                  Thread.sleep(1000 / UPDATE_RATE);
               } catch (InterruptedException ex) {}
            }
         }
		};
		bounceThread.start();
	}
	private class resizeHandler extends ComponentAdapter
	{
		public void componentResized(ComponentEvent e) {
			
		   for (int i = 0; i < ballCount; i++){
				ballArray[i].setBounds( BallPanel.this.getSize() );
				
			}        
		}
	}
	
	
	boolean isMouseDragged = false;
	int x1, x2;
	int y1, y2;
	float theta_radians;
	Random randumNum = new Random();
	private class MyMouseListener extends MouseMotionAdapter
	{
		public void mouseDragged( MouseEvent e )
		{
			isMouseDragged = true;
			x2 = e.getX();
			y2 = e.getY();
		}
		
	}
		private class MouseReleaseHandler extends MouseAdapter
	{
		

	
		
		public void mousePressed(MouseEvent e) {
			x1 = e.getX();
			y1 = e.getY();
		}
		
		
		public void mouseReleased( MouseEvent e )
		{
			try {
				createBall();
			}catch ( Exception ex){
				System.err.println("Somethings wrong...");
			}
			isMouseDragged = false;
		}
		
	}
	
	public void createBall(){
	
		int delta_x = x1 - x2;
		int delta_y = y2 - y1;
		theta_radians = (float) Math.atan2(delta_y, delta_x);
		float distance = (float) ( Math.sqrt( (x1-x2)*(x1-x2) +  (y1-y2)*(y1-y2)) * (0.05) );
		float speed = 10;
			
		if ( distance < 5){ 
			speed = 10;			
		} else if( distance > 20){
			speed = 20;
		} else {
			speed = distance;
		}
		
		if(ballCount < 1000 && isMouseDragged)
		{
			
			if( ballStarted == false) {
				startBounce();
				ballStarted = true;		
			}
				
			ballArray[ballCount] = new Ball(ballCount,x1,y1, speed, theta_radians , randumNum.nextInt(40)+10, this.getSize() );
			threadExecutor.execute( ballArray[ballCount] );
			setStatusMessage("Ball #" + (ballCount + 1) + " added!" + " -- Speed: " + speed);
			ballCount++;
			//System.err.println(BallPanel.this.getWidth() +":"+ BallPanel.this.getHeight());
				
		}
	}
	
	public JLabel createStatusBar(){
      
      statusBar = new JLabel("Click and Drag to Begin.");
      statusBar.setOpaque(true);
      	
      return statusBar;
   }
	
	public void setStatusMessage(String message)
   {
         statusMessage = message;
         statusBar.setText(message);
   }
	
	public Ball[] getBallArray(){
		return ballArray;
	}
}//End BallPanel Class
/********************************************************************

********************************************************************/
class Ball implements Runnable
{
	float x,y,speedY, speedX, angle,radius;
	float ballMaxX, ballMaxY;
	double boundingRectangle;
	Random randomNumber = new Random();
	Color randomColor;
	int ballID;
	
	private Ball[] ballArray;
	
	public Ball (int ballID,float x,float y,float speed,float rad,float radius, Dimension d){
		this.ballID = ballID;
		this.x = x;
		this.y = y;
		this.angle = angle;
		this.radius = radius;
		//calcBoundry(radius, d);
		
		setBounds(d);
      this.speedX = (float)(speed * Math.cos(rad));
      this.speedY = (float)(-speed * (float)Math.sin(rad));
		rollColor();
		
		
	}	
	public void rollColor(){
		randomColor = new Color(randomNumber.nextInt(256),randomNumber.nextInt(256),randomNumber.nextInt(256));
	}
	public void setBounds(Dimension d)
	{
		ballMaxY = (float) d.getHeight() - radius;
		ballMaxX = (float) d.getWidth() - radius;
	}

	
	public void updatePosition(){
		x += speedX;
		y += speedY;
		
		if( y < radius){
			speedY = -speedY;
			y = radius;
			rollColor();
		} else if ( y > ballMaxY) {
			speedY = -speedY;
			y = ballMaxY;
			rollColor();
		}
		if( x < radius){
			speedX = -speedX;
			x = radius;
			rollColor();
		} else if ( x > ballMaxX) {
			speedX = -speedX;
			x = ballMaxX;
			rollColor();
		}	
		ballToBallCollisionDetection();	
	}
	
	public void ballToBallCollisionDetection(){
		
	}
	public void draw(Graphics g)
	{
		
		g.setColor(randomColor);
		g.fillOval((int)(x - radius), (int)(y - radius), (int)(2 * radius), (int)(2 * radius));
	}
	
	public void run()
	{
		updatePosition();
		
	}
	
	public void setBallArray(Ball[] bArr)
	{
		ballArray = bArr;
	}
	
	
}//End Ball Class

class BoundingRectangle
{
	int x1,x2,y1,y2;
	
	public BoundingRectangle( int x1, int x2, int y1, int y2)
	{
		this.x1 = x1;
		this.x2 = x2;
		this.y1 = y1;
		this.y2 = y2;
	}
	
	public int getX1(  ){
		return x1;
	}
	
	public int getX2(  ){
		return x2;
	}
	
	public int getY1( ){
		return y1;
	}
	public int getY2( ){
		return y2;
	}

}