//Alex Gian
//1075979
//Assignment ImageZoom 
//This program allows the user to open a file and resize it. 
//Also, the user can fit the image to the window.
 
	//Alot of packages
	//Image and File 
	import javax.imageio.ImageIO;
	import java.io.IOException;
	import java.awt.image.BufferedImage;
	
	import java.io.File;
   import javax.swing.filechooser.FileNameExtensionFilter;
	import javax.swing.JFileChooser;

	//GUI
	import java.awt.Dimension;
	import java.awt.Graphics2D;
	import java.awt.Graphics;
	import java.awt.Color;
	
   import javax.swing.JScrollPane;
	import javax.swing.JPanel;
	import javax.swing.JFrame;
	import javax.swing.JLabel;
	
	//Layouts, Buttons, and Menu
	
	import java.awt.BorderLayout;
	import javax.swing.BoxLayout;
	import javax.swing.BorderFactory;

	import javax.swing.JMenuItem;
	import javax.swing.JCheckBoxMenuItem;
	import javax.swing.JMenu;
	import javax.swing.JMenuBar;
	import javax.swing.JSlider;
	
	import java.awt.Component;
	
	//Events
	import javax.swing.KeyStroke;
	import java.awt.event.KeyEvent;
	
	import java.awt.event.ItemListener;
	import java.awt.event.ItemEvent;
	
	//Check box that controls the sliderPanel
	import javax.swing.event.ChangeEvent;
	import javax.swing.event.ChangeListener;
	
	//Menu Button press
	import java.awt.event.ActionListener;
   import java.awt.event.ActionEvent;
	
	
	
   public class ImageZoom extends JPanel {
          
      private BufferedImage img, modImg;
      private JFileChooser fc;
      private String statusMessage, fileName;
      private  JLabel statusBar;
      private JPanel sliderPanel;
      private boolean isCustomScaleActivated;
      private JScrollPane scrollPane;
   	
      public void paintComponent( Graphics g )
      {
         super.paintComponent( g );
         this.setBackground( Color.WHITE );
         g.drawImage(modImg, 0, 0, null);
      }
   
      public ImageZoom() {
      	//Create the file chooser
         fc = new JFileChooser();//File chooser learnt from one Oracle's tutorials.
         //Found how to set home directory to the one that's running the program from a forum "programmersheaven.com"
			fc.setCurrentDirectory( new File(System.getProperty("user.dir")) );
      	//FileNameExtensionFilter from the documentation.
         FileNameExtensionFilter myFilter = new FileNameExtensionFilter("PNG, GIF, and JPG Files", "PNG", "png", "GIF", "gif", "JPG", "jpg", "jpeg");
         fc.setFileFilter(myFilter);
      }
		
		//Put the entire imageZoom class into a scrollPane and return it. 
		//Because I was having trouble with revalidating the scrollpane after adding the image and/or scaling. The scroll
		// pane did not appear until I resized the window. Also, I did this so I didn't need to recode everything.
      public JScrollPane imageZoomScroll()
      {
         scrollPane = new JScrollPane(this);
         return scrollPane;
      }
   	
      private class sliderHandler implements ChangeListener  
      {
         public void stateChanged(ChangeEvent e) {
            JSlider source = (JSlider)e.getSource();
            int scale = (int)source.getValue();
         	//System.out.println(source.getValueIsAdjusting());
            modImg = resizeImage(img,scale);
            ImageZoom.this.repaint();
         }
      }
   
		public JPanel sliderBar()
      {
         //Slider from http://docs.oracle.com/javase/tutorial/displayCode.html?code=http://docs.oracle.com/javase/tutorial/uiswing/examples/components/SliderDemoProject/src/components/SliderDemo.java
         sliderPanel = new JPanel();
         sliderPanel.setLayout(new BoxLayout(sliderPanel, BoxLayout.PAGE_AXIS));
      	
         JLabel sliderLabel = new JLabel("Scale (%)", JLabel.CENTER);
      	
         sliderLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
      	
         //sliderPanel.add(sliderLabel);
         sliderPanel.setVisible(false);
         sliderPanel.setOpaque(false);
         JSlider resizer = new JSlider(JSlider.HORIZONTAL,0, 200, 100);
         sliderPanel.add(resizer);
      	
      	//Listen to slider
         resizer.addChangeListener(new sliderHandler() );			
         
         sliderPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
         add(sliderPanel, BorderLayout.NORTH);
         
         return sliderPanel;
      }
   	
      public JMenuBar createMenuBar(){//This is based on Orcale way to set the menu bar by using the JFrame's setJMenuBar method and calling this method, createMenuBar.
         JMenuBar menuBar;
         JMenu scaleMenu, fileMenu;
         JMenuItem miScale50,resetScale, closeFile, miCustomScale, openButton, scaleToWindowItem;
         JCheckBoxMenuItem cbCustomScale;
      	//Create MenuBar
         menuBar = new JMenuBar();
      	
      	//Menu for File
         fileMenu = new JMenu("File");

         menuBar.add(fileMenu);
       	//Items/Submenus for File Menu  
         openButton = new JMenuItem("Open");
			openButton.setMnemonic(KeyEvent.VK_O);
			openButton.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.ALT_MASK));
         fileMenu.add(openButton);
         closeFile = new JMenuItem("Close");
			closeFile.setMnemonic(KeyEvent.VK_C);
			closeFile.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.ALT_MASK));
         fileMenu.add(closeFile);
         closeFile.addActionListener(
               new ActionListener()        
               {
                  public void actionPerformed( ActionEvent e )
                  {  
                     modImg = null;  
                     ImageZoom.this.repaint(); 
                  }
               }
            
            );      
      
         openButton.addActionListener(
               new ActionListener()        
               {
                  public void actionPerformed( ActionEvent e )
                  {   
                     int returnVal = fc.showOpenDialog(ImageZoom.this);
                                       	
                     if (returnVal == JFileChooser.APPROVE_OPTION) {
                        File file = fc.getSelectedFile();
                        loadImage(file);             	
                        
                     	
                     } 
                     else {
                        setStatusMessage("Open command cancelled by user.");
                     }
                     
                  } 
               });
      
      	//Menu for Scale
         scaleMenu = new JMenu("Scale");
         menuBar.add(scaleMenu);
      	
      	//50% Item
         miScale50 = new JMenuItem("50%");
         scaleMenu.add(miScale50);
         
      	//Fit to window Item
         scaleToWindowItem = new JMenuItem("Fit to Window");
			scaleToWindowItem.setMnemonic(KeyEvent.VK_F);
			scaleToWindowItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, ActionEvent.ALT_MASK));
         scaleMenu.add(scaleToWindowItem);
      	
      	//Checkbox Item
         cbCustomScale = new JCheckBoxMenuItem("Custom Scale");
         scaleMenu.add(cbCustomScale);
         
         //Reset Item
         resetScale = new JMenuItem("Reset");
         scaleMenu.add(resetScale);
         
      
      	//Add Handler to fit to window
      	
         scaleToWindowItem.addActionListener(  
               new ActionListener()
               {
                  public void actionPerformed(ActionEvent e) {

                     int frameW = scrollPane.getWidth();//I tried using the ImageZoom.this.getWidth but it returns Width that was adjusted to the image so that did not work.
                     int frameH = scrollPane.getHeight();//I had to use the width and height the user actually saw to fit to the window.
                     int imgW = img.getWidth();
                     int imgH = img.getHeight();
                  
                     double frameRatio = (double)frameW/frameH;
                     double imageRatio = (double)imgW/imgH;
                     double scaleRatioToFit;

                     if(frameRatio > imageRatio){//Height is smaller so fit to height
                        scaleRatioToFit = (double)frameH/imgH;
                     } 
                     else {//Width is smaller so fit to width
                        scaleRatioToFit = (double)frameW/imgW;
                     }
                  
                     double tolerance = 0.001;//make the image slightly smaller because narrowing conversion creating an error
                     modImg = resizeImage( img, (int)( (scaleRatioToFit - tolerance)* 100) );
                     ImageZoom.this.repaint();
                  	
                  	String message	= "Fit to Window. " + statusMessage;          
                     setStatusMessage(message);
                  
                  }
               });
      	//Add Handler to Custom Scale
         cbCustomScale.addItemListener(  
               new ItemListener()
               {
                  public void itemStateChanged(ItemEvent e) {
                  //JMenuItem source = (JMenuItem)(e.getSource());
                  
                     String stateString;
                     if (e.getStateChange() == ItemEvent.SELECTED){
                        stateString = "selected.";
                        isCustomScaleActivated = true;
                        ImageZoom.this.sliderPanel.setVisible(isCustomScaleActivated);
                     }
                     else{ 
                        stateString = "unselected.";
                        isCustomScaleActivated = false;
                        ImageZoom.this.sliderPanel.setVisible(isCustomScaleActivated);
                     }
                  
                     setStatusMessage("Custom Scale is " + stateString );
                  }
               });
      	
      	//Add Handler to Scale 50%
         miScale50.addActionListener(
               new ActionListener()        
               {
                  public void actionPerformed( ActionEvent e )
                  {  
                     
                     if(!isCustomScaleActivated){
                        modImg = resizeImage(img,50);
                        ImageZoom.this.repaint();
                     }
                  } 
               });
      
      	//Add Handler for Reset
         resetScale.addActionListener(
               new ActionListener()        
               {
                  public void actionPerformed( ActionEvent e )
                  {  
                     
                     if(!isCustomScaleActivated){
                        modImg = resizeImage(img,100);
                        ImageZoom.this.repaint();
                     }
                  } 
               });
      
      	
      	//Return Menu Bar 
         return menuBar;
      }	
		
		//This is called by the file chooser when it gets the file. I should make it private since its called indirectly.
      private void loadImage(File f)
      {
         try {	 	
            img = ImageIO.read(f);
            modImg = img;
            
            this.setPreferredSize(new Dimension(img.getWidth(),img.getHeight())); 
            scrollPane.revalidate();
         	 
            this.repaint();
            setStatusMessage("Opening: " + f.getAbsolutePath() + " Width: " + modImg.getWidth() + " Height: " + modImg.getHeight() );
         } 
            catch (IOException e) {
               setStatusMessage("Could not load file.");
            }
      	
      } 
   	
      public JLabel createStatusBar(){
      
         statusBar = new JLabel("Ready.");
         statusBar.setOpaque(true);
      	
         return statusBar;
      }
   	
      public void setStatusMessage(String message)
      {
         statusMessage = message;
         statusBar.setText(message);
      }
   
      private  BufferedImage resizeImage(BufferedImage orgImg, int scale)
      {
         scale = (scale <= 0) ? 1 : scale;
         
         int w = (int)(orgImg.getWidth()* scale/100);
         int h = (int)(orgImg.getHeight()* scale/100);
         
         BufferedImage scaledImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
         Graphics2D g = scaledImage.createGraphics();
         g.drawImage(orgImg, 0, 0, w, h,null);
         g.dispose();
         
         
         this.setPreferredSize(new Dimension(w,h));
         scrollPane.revalidate();//After adjusting the preferred size I need to update the scroll pane. 
         //I had a problem with scroll bars not appear after readjusting scale and/or open the file. 
         //Thanks to people from stackoverflow.
         
         
         setStatusMessage("Scale: " + scale + "% " +" Width: " + w + " Height: " + h); 
         return scaledImage; 
      
      }
   	
      public static void main(String args[])
      {
         JFrame frame = new JFrame("Image Zoom ver1.0");
         frame.setLayout(new BorderLayout() );
         ImageZoom app = new ImageZoom();
         
         frame.setJMenuBar(app.createMenuBar()); 
         frame.add(app.sliderBar(),BorderLayout.NORTH);
         frame.add(app.imageZoomScroll(),BorderLayout.CENTER);	
         frame.add(app.createStatusBar(),BorderLayout.SOUTH);
      		
         frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         frame.setSize(640,480);
         frame.setVisible( true );
      
      }	
   }

